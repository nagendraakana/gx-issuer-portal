import Vue, { PluginObject } from 'vue'

import { ethers, providers, Signer } from 'ethers'
import { EthersFunctions, EthersjsPluginOptions } from '~/types/ethers'

const plugin: PluginObject<EthersjsPluginOptions> = {
  // eslint-disable-next-line @typescript-eslint/no-shadow
  install: (Vue, _options?: EthersjsPluginOptions) => {
    let provider: providers.Web3Provider
    let signer: Signer

    function getProvider() {
      return provider
    }

    function setProvider(windowProvider: providers.ExternalProvider) {
      provider = new ethers.providers.Web3Provider(windowProvider)
      return ethers
    }

    async function connect(): Promise<string> {
      const account = await provider.send('eth_requestAccounts', [])
      return account || ''
    }

    function getSigner(): Signer {
      if (signer == null) {
        signer = provider.getSigner()
      }
      return signer
    }

    const ethersFns: EthersFunctions = {
      ethers,
      getProvider,
      setProvider,
      getSigner,
      connect
    }

    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$Web3 = ethersFns
  }
}

Vue.use(plugin)
