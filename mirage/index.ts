// eslint-disable-next-line import/no-extraneous-dependencies
import { Server, Model, JSONAPISerializer } from 'miragejs'
// eslint-disable-next-line import/no-extraneous-dependencies
import { faker } from '@faker-js/faker'

// eslint-disable-next-line import/prefer-default-export
export function createServer() {
  faker.seed(42)

  const accounts = [
    { username: faker.internet.userName(), email: faker.internet.email() },
    { username: faker.internet.userName(), email: faker.internet.email() }
  ]

  // eslint-disable-next-line no-new
  new Server({
    fixtures: {
      accounts
    },
    models: {
      account: Model
    },
    serializers: {
      application: JSONAPISerializer
    },
    routes() {
      this.namespace = 'api'
      this.urlPrefix = 'http://localhost:3000'

      // this.resource('accounts')
      this.get('/accounts')
      this.get('/accounts/:id')
      this.post('/accounts')
      this.patch('/accounts/:id')
      this.del('/accounts/:id')

      this.post('/activate', (_schema, request) => {
        const { activationLink } = JSON.parse(request.requestBody)

        if (!activationLink || !activationLink?.startsWith('123')) {
          return false
        }

        return true
      })

      this.get('/verifier-api/auth', (_schema, request) => {
        const { accessToken } = JSON.parse(request.requestBody)

        if (!accessToken || !accessToken?.startsWith('123')) {
          return false
        }

        return true
      })

      this.get('/verifier-api/protected', (_schema, _request) => true)
    }
  })
}
