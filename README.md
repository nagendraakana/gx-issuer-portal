# Gaia-X Lab Onboarding Portal

- [**Get started**](#get-started)
  - [🔨&nbsp; Build Setup](#build-setup)
  - [📂&nbsp; Project Structure](#project-structure)
- [**Using the Onboarding Portal**](#using-the-onboarding-portal)
  - [🔒&nbsp; Creating a wallet account](#creating-a-wallet-account)
  - [🪪&nbsp; Registering a DID:WEB](#registering-a-didweb)
  - [🔑&nbsp; Importing a custom key pair](#optional-importing-a-custom-keypair)
  - [❔&nbsp; Requesting a Gaia-X Participant Credential](#requesting-a-gaia-x-participant-credential)
  - [✅&nbsp; Verifying a Gaia-X Participant Credential](#verifying-a-gaia-x-participant-credential)
<br/><br/>
  ---
<br/>

## Get started
### Build Setup

```bash
# install dependencies
$ npm install

## install prerequisites (husky)
npm run prepare

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

### Project Structure

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

#### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

#### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

#### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).

#### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

#### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

#### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

#### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).

## Using the Onboarding Portal

The Onboarding Portal is a Demonstrator developed in the [Gaia-X Lab](https://gitlab.com/gaia-x/lab/). It showcases how the issuance of Verifiable Credentials can be linked to existing authentication mechanisms and how any third party might verify issued credentials.

Please use the step-by-step guide below to start issuing a Gaia-X Participant Credential using the Gaia-X Lab Onboarding Portal yourself.

### Creating a wallet account
Any participant in an SSI ecosystem needs a SSI wallet to hold Verifiable Credentials. Currently, the Onboarding Portal supports a [walt.id](https://walt.id/) SSI wallet which is hosted at [wallet.lab.gaia-x.eu](https://wallet.lab.gaia-x.eu)

You can login to the wallet by either providing your email address and a password or logging in via Metamask. Since the wallet instantiation is only used for demo and testing purposes, there is currently just a mock-up authentication. Your account information, therefore, will not be stored on our backends.

Once you are logged in to the wallet, you should see an empty screen or a list of any previously issued credentials.

<!-- Key import could also be removed for now. -->
### Optional: Importing a custom key pair
> **WARNING:** Please do note that the wallet should only be used for testing purposes. Please do not import any keys that are used in production!

In SSI ecosystems, a key pair is used to identify any participant. The walt.id wallet allows for custom key imports, to be able to reuse existing keys in the SSI ecosystem.

To import a custom key pair click on the burger-menu icon to open the wallet menu. You can then click on `View` next to `Data`, to be able to start the importing process with a click on `Import`. To import your key, prepare it in the `JWK` or `PEM` format and paste it into the textbox and click on `Import`.


### Registering a did:web
The next step is to create a decentralized identifier, a so called [DID](https://www.w3.org/TR/did-core/), representing your business in the SSI ecosystem. There are many different methods to create and resolve DIDs. However, in the Gaia-X Lab context, the [did:web Method](https://w3c-ccg.github.io/did-method-web/) was selected as the current default method to use.

The walt.id wallet supports creation of new did:webs out of the box. You can click on the burger-menu icon to open the wallet menu. Now click on `Settings`, followed by a click on `Ecosystems`. Here, the ecosystems `EBSI/ESSIF`, `DNS` and `key` are currently supported. Click on `Add` next to `DNS` to add a new did:web to your wallet.

After clicking on `Get started` to trigger the did:web creation process you have two options to generate your DID document:

#### 1. Provide a custom domain and host the JSON document yourself

If you want to host the DID document yourself or generate a DID for your company's domain, you can now enter a custom domain in the input field, e.g. `my-company.eu`. After clicking on `Generate DID` you can copy and paste the generated JSON to host it on the webserver of your company. To learn more about how a did:web is read and resolved, please refer to the [did:web Method Specification](https://w3c-ccg.github.io/did-method-web/#read-resolve).

#### 2. Utilize the wallet registry to take care of hosting the JSON document

If you do not want to host the did:web yourself, you can create a did document without providing a custom domain. Just hit the `Generate DID` button to generate your DID document, which will automatically be served via the wallet registry. 

### Requesting a Gaia-X Participant Credential
This step can be started in two different ways. If you were following the step-by-step guide above, you could request a credential from within your wallet (otherwise skip to below the following list):
1. Open the wallet menu
2. Click on `Credentials`
3. Click `+ Requeast Credential`
4. Select the `Gaia-X Lab Onboarding Portal` issuer and the `ParticipantCredential`
5. Click on `Fetch Credential`
6. You will be redirected to the registration page with a sessionId for your wallet session

If you are just starting from this step, head over to the [Onboarding Registration Page](https://onboarding-portal.lab.gaia-x.eu/registration/).
To be able to receive a [Gaia-X Participant Credential](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/participant/) you need to have access to an email address that is already authorized to access the [Gaia-X GitLab group](https://gitlab.com/gaia-x).
1. Enter the email address that is authorized on GitLab and hit `Submit`
2. If your email address was found you will receive a registration mail shortly after
3. Click on the activation link provided in the registration mail
4. Pick the SSI wallet that you are using (currently only the [Gaia-X Lab walt.id wallet](https://wallet.lab.gaia-x.eu/) is supported)
5. Enter the information that should be issued in your verifiable credential
6. Click on `Issue Credential`
7. You are now redirected to the selected wallet where you can accept the newly issued credential

### Verifying a Gaia-X Participant Credential
To present and verify a Gaia-X Participant Credential, head to the [Onboarding Portal Verification Page](https://onboarding-portal.lab.gaia-x.eu/verification).
Here you can select the SSI wallet that holds your Gaia-X Participant credential. Please note that currently, only the [Gaia-X Lab walt.id wallet](https://wallet.lab.gaia-x.eu/) is supported. After clicking on `Connect Wallet` you will be redirected to the selected wallet to present your credential.

> If you currently do not have a Gaia-X Participant Credential in your wallet you will be prompted to fetch a credential now and can follow the [Request Credential Steps above](#requesting-a-gaia-x-participant-credential)

Once you have the required credential in your wallet, you will be able to click on `Share` to present the credential to the Onboarding Verifier. You will be redirected to the Onboarding Portal, where you download the raw verifiable presentation. The page will also indicate whether or not the presentation could be successfully verified.